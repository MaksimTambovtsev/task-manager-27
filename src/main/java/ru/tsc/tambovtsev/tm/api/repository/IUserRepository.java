package ru.tsc.tambovtsev.tm.api.repository;

import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.model.User;

import java.util.List;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User removeUser(@Nullable User user);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeByLogin(@Nullable String login);

}
